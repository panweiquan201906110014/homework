package com.coursemanage.Polymorphism;


public class Application {
    public static void main(String[] args) {
        int k = (int) (Math.random()*3d);
        switch (k){
            case 0:
                Novel novel = new Novel();
                novel.showInformation();
                break;

                case 1:
                    Journal journal = new Journal();
                    journal.showInformation();
                    break;
                case 2:
                    Magazine magazine = new Magazine();
                    magazine.showInformation();
                    break;

                 case 3:
                     Textbook textbook = new Textbook();
                     textbook.showInformation();
                     break;

                }
            }
        }
    }
}
