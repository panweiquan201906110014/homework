package com.coursemanage.Polymorphism;

public class Novel extends Book{
    private int chapter;
    private int id;

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    @Override
    public Object getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Override
    public void showInformation(){
        System.out.println("这是小说嗷~");
    }
}