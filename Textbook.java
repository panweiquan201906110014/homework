package com.coursemanage.Polymorphism;

public class Textbook extends Book{
    private int chapter;
    private int price;

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    @Override
    public void showInformation(){
        System.out.println("这是课本嗷~");
    }
}
