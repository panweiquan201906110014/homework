package com.coursemanage.Polymorphism;

public class Journal extends Book {
    private int price;
    private int chapter;

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getChapter() {
        return chapter;
    }

    public void setChapter(int chapter) {
        this.chapter = chapter;
    }

    @Override
    public void showInformation() {
        System.out.println("这是杂志嗷");
    }
}