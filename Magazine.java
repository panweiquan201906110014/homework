package com.coursemanage.Polymorphism;

public class Magazine extends Book {
    private int price;
    private int id;

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public Object getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    @Override
    public void showInformation(){
        System.out.println("这是杂志嗷~");
    }
}
