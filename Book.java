package com.coursemanage.Polymorphism;

public class Book<string> {
    private int pages;
    private double price;
    private string id;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public string getId() {
        return id;
    }

    public void setId(string id) {
        this.id = id;
    }
    public void showInformation(){
        System.out.println("这是读物喔");
    }
}
